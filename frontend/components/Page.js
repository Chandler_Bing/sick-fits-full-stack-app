import React, { Component } from 'react';
import styled, { ThemeProvider, injectGlobal } from 'styled-components';
import { StyledPage, Inner, theme } from './styles/PageStyles';
import Header from './Header';
import Meta from './Meta';
injectGlobal`
@font-face {
  font-family: 'radnika_next';
  src: url('/static/radnikanext-medium-webfont.woff2')format('woff2');
  font-weight: normal;
  font-style: normal
}
html{
  box-sizing: border-box;
  font-size: 10px;
}
*, *:before, *:after{
  box-sizing: inherit;
}
body{
  padding: 0;
  margin: 0;
  font-size: 1.5rem;
  font-family: 'radnika_next';
  line-height: 2;
}
a{
  text-decoration: none;
  color: ${theme.black};
}
`;
export default class Page extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <StyledPage>
          <Meta />
          <Header />
          <Inner>{this.props.children}</Inner>
        </StyledPage>
      </ThemeProvider>
    );
  }
}
