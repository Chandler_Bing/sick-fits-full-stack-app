import React from 'react';
import PaginationStyles from './styles/PaginationStyles';
import gql from 'graphql-tag';
import Head from 'next/head';
import Link from 'next/link';

import { Query } from 'react-apollo';
import ErrorMessage from './ErrorMessage';
import { perPage } from '../config';

const PAGINATION_QUERY = gql`
  query PAGINATION_QUERY {
    itemsConnection {
      aggregate {
        count
      }
    }
  }
`;
export default function Pagination(props) {
  return (
    <Query query={PAGINATION_QUERY}>
      {({ data, loading, error }) => {
        const {
          itemsConnection: {
            aggregate: { count },
          },
        } = data;
        const { page } = props;
        const pages = Math.ceil(count / perPage);
        if (error) return <ErrorMessage error={error} />;
        if (loading) return <p>loading...</p>;
        return (
          <PaginationStyles>
            <Head>
              <title>
                Sick Fits! | Page {page} of {pages}
              </title>
            </Head>
            <Link
              prefetch
              href={{
                pathname: '/items',
                query: { page: page - 1 },
              }}
            >
              <a aria-disabled={page <= 1}>← Prev</a>
            </Link>

            <p>
              Page {props.page} of {pages}
            </p>
            <p>{count} Items Total</p>
            <Link
              prefetch
              href={{
                pathname: '/items',
                query: { page: page + 1 },
              }}
            >
              <a aria-disabled={page >= pages}>Next →</a>
            </Link>
          </PaginationStyles>
        );
      }}
    </Query>
  );
}
