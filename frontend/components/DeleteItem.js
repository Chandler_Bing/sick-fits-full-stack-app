import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import { ALL_ITEMS_QUERY } from './Items';
export const DELETE_ITEM_MUTATION = gql`
  mutation DELETE_ITEM_MUTATION($id: ID!) {
    deleteItem(id: $id) {
      id
    }
  }
`;
export default class DeleteItem extends Component {
  // this function runs after deleting an item in order to remove the deleted item from the apollo cache && update the UI
  update = (cache, payload) => {
    //manually update the cache on the client so it matches the server

    // 1. read the chache for the items we want
    const data = cache.readQuery({ query: ALL_ITEMS_QUERY });
    console.log({ cache, payload, data });
    // 2. filter the deleted item out of the page
    data.items = data.items.filter(
      item => item.id !== payload.data.deleteItem.id,
    );
    // 3. put the items back!
    cache.writeQuery({ query: ALL_ITEMS_QUERY, data });
  };
  handleClick = (e, deleteItem) => {
    if (confirm('Are you sure you want to delete this')) deleteItem();
  };
  render() {
    return (
      <Mutation
        mutation={DELETE_ITEM_MUTATION}
        variables={{ id: this.props.id }}
        update={this.update}
      >
        {deleteItem => (
          <button onClick={e => this.handleClick(e, deleteItem)}>
            {this.props.children}
          </button>
        )}
      </Mutation>
    );
  }
}
