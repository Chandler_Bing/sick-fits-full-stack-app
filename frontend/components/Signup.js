import React, { Component } from 'react';
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';
import Form from './styles/Form';
import ErrorMessage from './ErrorMessage';
const SIGNUP_MUTATION = gql`
  mutation SIGNUP_MUTATION(
    $email: String!
    $name: String!
    $password: String!
  ) {
    signup(email: $email, name: $name, password: $password) {
      id
      email
      name
    }
  }
`;
export default class Signup extends Component {
  state = {
    name: '',
    email: '',
    password: '',
  };
  saveToState = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleSubmit = async (e, signup) => {
    e.preventDefault();
    await signup();
    this.setState({ name: '', email: '', password: '' });
  };
  render() {
    return (
      <Mutation mutation={SIGNUP_MUTATION} variables={this.state}>
        {(signup, { error, loading }) => {
          return (
            <Form method="POST" onSubmit={e => this.handleSubmit(e, signup)}>
              <fieldset disabled={loading} aria-busy={loading}>
                <h2>Sign Up for An Account</h2>
                <ErrorMessage error={error} />
                <label htmlFor="email">
                  Email
                  <input
                    onChange={this.saveToState}
                    type="email"
                    name="email"
                    value={this.state.email}
                    placeholder="Email"
                  />
                </label>
                <label htmlFor="name">
                  Name
                  <input
                    onChange={this.saveToState}
                    type="text"
                    name="name"
                    value={this.state.name}
                    placeholder="Name"
                  />
                </label>
                <label htmlFor="password">
                  Password
                  <input
                    onChange={this.saveToState}
                    type="password"
                    name="password"
                    value={this.state.password}
                    placeholder="Password"
                  />
                </label>
                <button type="submit">Sign Up!</button>
              </fieldset>
            </Form>
          );
        }}
      </Mutation>
    );
  }
}
