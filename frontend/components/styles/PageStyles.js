import styled from 'styled-components';

export const theme = {
  red: '#ff0000',
  black: '#393939',
  grey: '#3a3a3a',
  lightGrey: '#e1e1e1',
  offWhite: 'ededede',
  maxWidth: '1000px',
  bs: '0 12px 24px 0 rgba(0,0,0,0.09)',
};
theme.lightgrey = theme.lightGrey;
export const StyledPage = styled.div`
  background: white;
  color: ${({ theme }) => theme.black};
`;
export const Inner = styled.div`
  max-width: ${({ theme }) => theme.maxWidth};
  margin: 0 auto;
  padding: 2rem;
`;
