import React, { Component } from 'react';
import gql from 'graphql-tag';
import { Query } from 'react-apollo';
import ErrorMessage from './ErrorMessage';
import { SingleItemStyles } from './styles/SingleItemStyles';
import Head from 'next/head';
const SINGLE_ITEM_QUERY = gql`
  query SINGLE_ITEM_QUERY($id: ID!) {
    item(where: { id: $id }) {
      id
      title
      description
      largeImage
    }
  }
`;
export default class SingleItem extends Component {
  render() {
    return (
      <Query query={SINGLE_ITEM_QUERY} variables={{ id: this.props.id }}>
        {({ error, loading, data }) => {
          if (error) return <ErrorMessage error={error} />;

          if (loading) return <p>Loading...</p>;

          if (!data.item) {
            return (
              <p>
                404: No item found with for <strong>{this.props.id}</strong>
              </p>
            );
          }
          const {
            item: { title, description, largeImage },
          } = data;
          return (
            <SingleItemStyles>
              <Head>
                <title>Sick Fits | {title} </title>
              </Head>
              <img src={largeImage} alt={title} />
              <div className="details">
                <h2>Viewing {title}</h2>
                <p>{description}</p>
              </div>
            </SingleItemStyles>
          );
        }}
      </Query>
    );
  }
}
