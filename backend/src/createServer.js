const { GraphQLServer } = require('graphql-yoga');

const Mutation = require('./resolvers/Mutation');
const Query = require('./resolvers/Query');
const db = require('./db');

// Create the GraphQL server

function createServer() {
  return new GraphQLServer({
    typeDefs: 'src/schema.graphql', // type definitions for the graphql server
    resolvers: {
      Mutation,
      Query,
    },
    resolverValidationOptions: {
      requireResolversForResolveType: false, //in order to get rid of some errors
    },
    context: req => ({ ...req, db }), //in order to access the database from resolvers, and we can do that via context, by maping the db onto every request
  });
}

module.exports = createServer;
