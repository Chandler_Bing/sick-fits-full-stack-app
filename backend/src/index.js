const cookieParser = require('cookie-parser');
const jwt = require('jsonwebtoken');
require('dotenv').config({ path: 'variables.env' });
const createServer = require('./createServer');
const db = require('./db');

const server = createServer();

// use express middleware to handle cookies (JWT)
server.express.use(cookieParser());
//  decode the JWT to get user id on every request
server.express.use((req, res, next) => {
  const { token } = req.cookies;
  if (token) {
    const userId = jwt.verify(token, process.env.APP_SECRET);
    //put the userID on the request object
    req.userId = userId;
  }
  next();
});
// TODO: use express middleware to populate current user

server.start(
  {
    cors: {
      // so nobody else, except our frontend, can hit our server endpoint
      credentials: true,
      origin: process.env.FRONTEND_URL,
    },
  },
  deets => {
    console.log(`Server is running on http://localhost:${deets.port}`);
  },
);
