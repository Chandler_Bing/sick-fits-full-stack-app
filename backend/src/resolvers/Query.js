// Query resolvers pull data from db
const { forwardTo } = require('prisma-binding');
const Query = {
  // async items(parent, args, ctx, info) {
  //   // item() is provided by prisma in src/generated/prisma.graphql
  //   const items = await ctx.db.query.items();
  //   return items;
  // },
  // same result but less verbose
  items: forwardTo('db'),
  item: forwardTo('db'),
  itemsConnection: forwardTo('db'),
  me(parent, args, ctx, info) {
    console.log(ctx.request.userId);
    if (!ctx.request.userId) {
      return null;
    }
    return ctx.db.query.user(
      {
        where: { id: ctx.request.userId.userId },
      },
      info,
    );
    // check if there is a current user id
    // if (!userId) return null;
  },
};

module.exports = Query;
