// This file connects to the remote prisma DB and gives us the ability to query it with JS

const { Prisma } = require('prisma-binding');

const db = new Prisma({
  typeDefs: 'src/generated/prisma.graphql', // it needs to know about all the types and mutations prisma generated in order to use it
  endpoint: process.env.PRISMA_ENDPOINT, //prisma server endpoint
  secret: process.env.PRISMA_SECRET,
  debug: false, //console.logs information, turn on when needed 'cause it's noisy
});

module.exports = db;
